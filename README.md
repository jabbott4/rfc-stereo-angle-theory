# Welcome to Stereo Angle Theory!

Developed in Java in the Processing IDE, this graphical executable proves that two sensors, 
namely cameras, enable a sense of depth and a collection of complex data more than any of the
group's individual parts.

![Image](https://gitlab.com/jabbott4/rfc-stereo-angle-theory/raw/master/screenshot_1.png)

1. The "cameras" calculate a yaw-angle to some target in the distance, and from that
trigonometry can reveal two lines (given some known displacemen between the cameras).

2. Solving for the intersection of those lines gives a point in 2D space, relative
to how the camera's positions are defined (ex. whether or not the left camera is 0, 0)

3. From that point in 2D space, distance from each of the cameras can be calculated.
Also, a yaw-angle-differential can be calculated from the perpendicular to the plane 
(in our case, a robot) that contains both cameras. 

## How to run? (Windows only)

Download the respective 32 bit or 64 bit folder, and run the executable inside.