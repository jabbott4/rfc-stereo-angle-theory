int[] cam1loc = {0, 480};
int[] cam2loc = {480, 480};
int cam_dim = 10;

int[] cam1cent = {cam1loc[0] + cam_dim/2, cam1loc[1] - cam_dim/2};
int[] cam2cent = {cam2loc[0] - cam_dim/2, cam2loc[1] - cam_dim/2};

void setup() {
  size(480, 480);
}

void draw() {
  background(200);
  scene();
  float[] slopes = camrays();
  float[] angles = yawangles(slopes);
  
  //now time to deduce mouse position based on only yaw angles from known
  int deltacamx = cam2cent[0] - cam1cent[0];
  //assume cam 1 cent is origin
  //y = m_1 * x
  //assume cam 2 is deltacamx to the right
  //y = m_2 * (x + deltacamx)
  //m_1 * x = m_2 * x + m_2 * deltacamx
  //x(m_1 - m_2) = m_2 * deltacamx
  //x = (m_2 * deltacamx)/(m_1 - m_2)
  float m_1 = toSlope(angles[0]);
  float m_2 = toSlope(angles[1]);
  drawnewslopes(m_1, m_2);
  //edit: works!!!!!
  
  float x = -(m_2 * deltacamx)/(m_1 - m_2);
  float y = m_1 * x;
  
  text(x + " + " + cam_dim/2 + " = " + (x + cam_dim/2), 0, 100);
  text(y + " + " + cam_dim/2 + " = " + (y + cam_dim/2), 0, 110);
  
  //now, calculate distances from camera...
  text("distance from left: " + (distance(x-cam1cent[0], y-cam1cent[0])), 0, 130);
  text("distance from right: " + (distance(x-cam2cent[0], y-cam2cent[0])), 0, 140);
  
  //okay, now we need heading differential and distance
  int centerx = width/2;
  //point is known now, so i will just use mouseX and mouseY for easy
  line(centerx, height, mouseX, mouseY);
  line(centerx, height, centerx, 0);
  float yaw_angle = atan((float)(mouseX - centerx) / realy(mouseY)-realy(height)) * 180 / PI;
  text("yaw_angle: " + yaw_angle, 0, 160);
}

float distance(float x, float y) {
  return sqrt(x*x + y*y);
}
void scene() {
  text(mouseX, 0, 10);
  text(realy(mouseY), 0, 20);
  rect(cam1loc[0], cam1loc[1]-cam_dim, cam_dim, cam_dim); 
  rect(cam2loc[0]-cam_dim, cam2loc[1]-cam_dim, cam_dim, cam_dim);
}

void drawnewslopes(float x, float y) {
  text(x, 0, 70);
  text(y, 0, 80);
}
float[] camrays() {
  //returns slopes
  line(cam1cent[0], cam1cent[1], mouseX, mouseY);
  line(cam2cent[0], cam2cent[1], mouseX, mouseY);
  float[] slopes = {0, 0};
  try{
    slopes[0] = (float)(realy(mouseY) - realy(cam1cent[1]))/(mouseX - cam1cent[0]); //flipped because of how processing
    slopes[1] = (float)(realy(mouseY) - realy(cam2cent[1]))/(mouseX - cam2cent[0]); //graphs from top to down
    text("slope 0: " + slopes[0], 0, 40);
    text("slope 1: " + slopes[1], 0, 50);
  } catch (Exception e) {
    
  }
  return slopes;
}

int realy(int in) {
  return height - in;
}

float[] yawangles(float[] slopes) {
  float[] yawangles = {
    atan(1.0/(float)slopes[0]) / PI * 180,
    atan(1.0/(float)slopes[1]) / PI * 180
  };
  text("" + yawangles[0], cam1cent[0] + 20, cam1cent[1]);
  text("" + yawangles[1], cam2cent[0] - 120, cam2cent[1]);
  return yawangles;
}

float toSlope(float angle) {
  float newangle = angle * PI / 180;
  return 1.0/tan(newangle);
}
